# ztags

ztags for [tagbar](https://github.com/preservim/tagbar)

## How to build

```sh
$ zig build --release
```

### Using Nix (Flakes)

If you have Nix with flakes enabled, you can build and install ztags directly from the repository:

```sh
$ nix profile install 'git+https://<repo url>'
```

Replace `<repo url>` with the actual URL of the Git repository.

Alternatively, you can build the project locally using:

```sh
$ nix build
```

This will create a result symlink in the current directory, pointing to the build output.

## How to use

Add `ztags` to your `PATH` and put the following configuration to your `.vimrc`.

```vim-script
let g:tagbar_type_zig = {
    \ 'ctagstype': 'zig',
    \ 'kinds' : [
        \'import:imports',
        \'const:constants',
        \'var:variables',
        \'field:fields',
        \'error:errors',
        \'enum:enum:1',
        \'union:union:1',
        \'struct:struct:1',
        \'opaque:opaque:1',
        \'function:functions',
        \'comptime:comptimes',
        \'test:tests',
    \ ],
    \ 'sro' : '.',
    \ 'kind2scope' : {
        \ 'enum' : 'enum',
        \ 'union' : 'union',
        \ 'struct' : 'struct',
        \ 'opaque' : 'opaque',
    \ },
    \ 'ctagsbin' : 'ztags',
    \ 'ctagsargs' : ''
\ }
```
