const std = @import("std");
const mem = std.mem;
const Ast = std.zig.Ast;
const Node = Ast.Node;
const ScopeList = std.ArrayList(struct {
    kind: []const u8,
    scope: []const u8,
});
const Error = std.fs.File.WriteError || error{OutOfMemory};

var ast: Ast = undefined;
var stack: ScopeList = undefined;
var filename: []const u8 = undefined;

var writer: std.io.BufferedWriter(4096, std.fs.File.Writer).Writer = undefined;
var allocator: mem.Allocator = undefined;

const Signature = struct {
    parm_list: []const u8,
    return_type: ?[]const u8 = null,
};

pub fn main() anyerror!void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer std.debug.assert(gpa.deinit() == .ok);

    allocator = gpa.allocator();
    const args = try std.process.argsAlloc(allocator);
    defer std.process.argsFree(allocator, args);

    if (args.len < 2) {
        std.debug.print("Usage: {s} file.zig\n", .{args[0]});
        return;
    }

    filename = args[1];
    const source = try std.fs.cwd().readFileAllocOptions(
        allocator,
        filename,
        std.math.maxInt(usize),
        null,
        @alignOf(u8),
        0,
    );
    defer allocator.free(source);

    const mode: Ast.Mode = if (std.mem.endsWith(u8, filename, ".zig")) .zig else .zon;
    ast = try Ast.parse(allocator, source, mode);
    defer ast.deinit(allocator);

    stack = ScopeList.init(allocator);
    defer stack.deinit();

    const stdout_file = std.io.getStdOut().writer();
    var bw = std.io.bufferedWriter(stdout_file);
    writer = bw.writer();

    if (mode == .zig) {
        try printTags(0); // print root node of Ast
    } else {
        const root_node_data = ast.nodes.items(.data)[0];
        try printTags(root_node_data.lhs);
    }
    try bw.flush();
}

fn printTags(index: Node.Index) !void {
    const tag = ast.nodes.items(.tag)[index];
    const main_token = ast.nodes.items(.main_token)[index];
    const data = ast.nodes.items(.data)[index];

    switch (tag) {
        .root => {
            // root ContainerMembers
            for (ast.rootDecls()) |member| {
                try printTags(member);
            }
        },

        .global_var_decl,
        .local_var_decl,
        .simple_var_decl,
        .aligned_var_decl,
        => var_decl: {
            const init_node = data.rhs;
            const public = if (ast.fullVarDecl(index).?.visib_token) |_| true else false;

            if (isContainer(init_node)) {
                try printContainer(main_token + 1, init_node, public, null);
                break :var_decl;
            }

            const init_node_tag = ast.nodes.items(.tag)[init_node];
            switch (init_node_tag) {
                .error_set_decl,
                .merge_error_sets,
                => {
                    try printLine(.{
                        .tag = main_token + 1,
                        .kind = "error",
                        .public = public,
                    });
                    break :var_decl;
                },

                .builtin_call_two => {
                    const init_node_main_token = ast.nodes.items(.main_token)[init_node];
                    const token_slice = ast.tokenSlice(init_node_main_token);
                    if (mem.eql(u8, token_slice, "@import") or mem.eql(u8, token_slice, "@cImport")) {
                        try printLine(.{
                            .tag = main_token + 1,
                            .kind = "import",
                            .public = public,
                        });
                        break :var_decl;
                    }
                },

                else => {},
            }

            // `var` or `const`
            try printLine(.{
                .tag = main_token + 1,
                .kind = ast.tokenSlice(main_token),
                .public = public,
            });
        },

        .fn_proto_simple,
        .fn_proto_multi,
        .fn_proto_one,
        .fn_proto,
        .fn_decl,
        => fn_decl: {
            var buf: [1]Node.Index = undefined;
            const full = ast.fullFnProto(&buf, index).?;
            const public = if (full.visib_token) |_| true else false;

            // get function signature
            const token_tags = ast.tokens.items(.tag);
            const l_paren = full.lparen;
            const r_paren = blk: {
                var n: isize = 0;

                break :blk for (token_tags[l_paren + 1 ..], l_paren + 1..) |token, i| {
                    switch (token) {
                        .l_paren => n += 1,
                        .r_paren => {
                            n -= 1;
                            if (n < 0) break i;
                        },
                        else => {},
                    }
                } else unreachable;
            };

            const token_starts = ast.tokens.items(.start);
            const l_start = token_starts[l_paren];
            const r_start = token_starts[r_paren];
            const parm_list = ast.source[l_start + 1 .. r_start];

            const last_token = ast.lastToken(full.ast.proto_node) + 1;
            const return_type = ast.source[r_start + 1 .. token_starts[last_token]];

            if (tag == .fn_decl) {
                // HACK: a function may return a struct, we treat it as a container
                const container_node: ?Node.Index = blk: {
                    const return_type_main_token = ast.nodes.items(.main_token)[full.ast.return_type];

                    if (std.mem.eql(u8, ast.tokenSlice(return_type_main_token), "type")) {
                        const block_node = ast.nodes.get(data.rhs);
                        const statements: []const Node.Index = switch (block_node.tag) {
                            .block_two,
                            .block_two_semicolon,
                            => if (block_node.data.rhs == 0)
                                &.{block_node.data.lhs}
                            else
                                &.{ block_node.data.lhs, block_node.data.rhs },

                            .block,
                            .block_semicolon,
                            => ast.extra_data[block_node.data.lhs..block_node.data.rhs],

                            else => unreachable,
                        };

                        const last_statement = ast.nodes.get(statements[statements.len - 1]);
                        if (last_statement.tag == .@"return" and isContainer(last_statement.data.lhs)) {
                            break :blk last_statement.data.lhs;
                        }
                    }
                    break :blk null;
                };

                if (container_node) |container| {
                    try printContainer(main_token + 1, container, public, parm_list);
                    break :fn_decl;
                }
            }

            try printLine(.{
                .tag = main_token + 1,
                .kind = "function",
                .public = public,
                .signature = .{
                    .parm_list = parm_list,
                    .return_type = return_type,
                },
            });
        },

        .container_field_init,
        .container_field_align,
        .container_field,
        => try printLine(.{
            .tag = main_token,
            .kind = "field",
        }),

        .test_decl => try printLine(.{
            .tag = if (data.lhs > 0) data.lhs else main_token,
            .kind = "test",
        }),

        .@"comptime" => try printLine(.{
            .tag = main_token,
            .kind = "comptime",
        }),

        .struct_init_one,
        .struct_init_one_comma,
        .struct_init_dot_two,
        .struct_init_dot_two_comma,
        .struct_init_dot,
        .struct_init_dot_comma,
        .struct_init,
        .struct_init_comma,
        => {
            for (structInitMembers(index)) |field_init| {
                try printStructMember(field_init);
            }
        },

        else => |unknown_tag| std.log.debug(
            "unknown: \x1b[33m{s}\x1b[m",
            .{@tagName(unknown_tag)},
        ),
    }
}

fn isContainer(node: Node.Index) bool {
    const tag = ast.nodes.items(.tag)[node];
    return switch (tag) {
        .container_decl_two,
        .container_decl_two_trailing,
        .container_decl,
        .container_decl_trailing,
        .container_decl_arg,
        .container_decl_arg_trailing,
        .tagged_union_two,
        .tagged_union_two_trailing,
        .tagged_union,
        .tagged_union_trailing,
        .tagged_union_enum_tag,
        .tagged_union_enum_tag_trailing,
        => true,

        else => false,
    };
}

fn printContainer(
    tag: Ast.TokenIndex,
    container: Node.Index,
    public: bool,
    parm_list: ?[]const u8,
) Error!void {
    const container_tag = ast.nodes.items(.tag)[container];
    const container_token = ast.nodes.items(.main_token)[container];
    const container_data = ast.nodes.items(.data)[container];
    const kind = ast.tokenSlice(container_token);

    // const A = `struct {}`, `union {}`, `enum {}` or `opaque {}`
    try printLine(.{
        .tag = tag,
        .kind = kind,
        .public = public,
        .signature = if (parm_list == null) null else .{ .parm_list = parm_list.? },
    });

    try stack.append(.{
        .kind = kind,
        .scope = ast.tokenSlice(tag),
    });
    defer _ = stack.pop();

    // print tags of their ContainerMembers
    const container_members: []const Node.Index = switch (container_tag) {
        .container_decl_two,
        .container_decl_two_trailing,
        .tagged_union_two,
        .tagged_union_two_trailing,
        => if (container_data.lhs == 0)
            &.{}
        else if (container_data.rhs == 0)
            &.{container_data.lhs}
        else
            &.{ container_data.lhs, container_data.rhs },

        .container_decl,
        .container_decl_trailing,
        .tagged_union,
        .tagged_union_trailing,
        => ast.extra_data[container_data.lhs..container_data.rhs],

        .container_decl_arg,
        .container_decl_arg_trailing,
        .tagged_union_enum_tag,
        .tagged_union_enum_tag_trailing,
        => blk: {
            const params = ast.extraData(container_data.rhs, Node.SubRange);
            break :blk ast.extra_data[params.start..params.end];
        },

        else => unreachable,
    };

    for (container_members) |member| {
        try printTags(member);
    }
}

inline fn structInitMembers(index: Node.Index) []const Node.Index {
    const node = ast.nodes.get(index);
    return switch (node.tag) {
        .struct_init_one,
        .struct_init_one_comma,
        => &.{node.data.rhs},

        .struct_init_dot_two,
        .struct_init_dot_two_comma,
        => &.{ node.data.lhs, node.data.rhs },

        .struct_init_dot,
        .struct_init_dot_comma,
        => ast.extra_data[node.data.lhs..node.data.rhs],

        .struct_init,
        .struct_init_comma,
        => blk: {
            const params = ast.extraData(node.data.rhs, Node.SubRange);
            break :blk ast.extra_data[params.start..params.end];
        },

        else => &.{},
    };
}

fn printStructMember(index: Node.Index) !void {
    const init_token = ast.firstToken(index) - 2;
    const members = structInitMembers(index);

    if (members.len == 0) {
        try printLine(.{
            .tag = init_token,
            .kind = "field",
        });
        return;
    }

    try printLine(.{
        .tag = init_token,
        .kind = "struct",
    });

    try stack.append(.{
        .kind = "struct",
        .scope = ast.tokenSlice(init_token),
    });
    defer _ = stack.pop();

    for (members) |member| {
        if (member == 0) break;
        try printStructMember(member);
    }
}

fn printLine(info: struct {
    tag: Ast.TokenIndex,
    kind: []const u8,
    public: bool = false,
    signature: ?Signature = null,
}) !void {
    const loc = ast.tokenLocation(0, info.tag);

    if (mem.eql(u8, info.kind, "comptime")) {
        try writer.print("comptime_{}", .{loc.line + 1});
    } else {
        try writer.print("{s}", .{ast.tokenSlice(info.tag)});
    }

    try writer.print(
        "\t{[file]s}\t{[line]};\"\t{[kind]s}\tline:{[line]}\tcolumn:{[column]}",
        .{
            .file = filename,
            .kind = info.kind,
            .line = loc.line + 1,
            .column = loc.column + 1,
        },
    );

    // write scopes
    if (stack.items.len > 0) {
        try writer.print("\t{s}:", .{stack.getLast().kind});

        try writer.print("{s}", .{stack.items[0].scope});
        for (stack.items[1..]) |scope| {
            try writer.print(".{s}", .{scope.scope});
        }
    }

    if (info.public) {
        try writer.writeAll("\taccess:public");
    }

    if (info.signature) |s| {
        const return_type_len = if (s.return_type) |rt| rt.len else 0;
        var buffer = try std.ArrayList(u8).initCapacity(allocator, @max(s.parm_list.len, return_type_len));
        defer buffer.deinit();
        const bw = buffer.writer();

        trim(bw, s.parm_list);
        try writer.print("\tsignature: ({s})", .{mem.trimRight(u8, buffer.items, ", ")});

        if (s.return_type) |rt| {
            buffer.clearRetainingCapacity();
            trim(bw, rt);
            try writer.print(" {s}", .{mem.trimRight(u8, buffer.items, " ")});
        }
    }

    try writer.writeByte('\n');
}

// trim newline and comments
fn trim(w: std.ArrayList(u8).Writer, string: []const u8) void {
    var it = mem.tokenizeScalar(u8, string, '\n');
    while (it.next()) |line| {
        const end = mem.indexOf(u8, line, "//") orelse line.len;
        const str = mem.trim(u8, line[0..end], &std.ascii.whitespace);
        if (str.len == 0) continue;

        w.print("{s} ", .{str}) catch unreachable;
    }
}
