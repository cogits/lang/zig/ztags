{
  description = "ztags for tagbar";
  inputs.nixpkgs.url = "flake:nixpkgs";

  outputs = {self, nixpkgs}: let
    inherit (nixpkgs) lib;

    eachSystem = systems: fn:
      lib.foldl' (acc: sys:
          lib.recursiveUpdate acc (lib.mapAttrs (_: value: {${sys} = value;}) (fn sys))
      ) {}
      systems;

    supportedSystems = ["x86_64-linux" "aarch64-linux" "x86_64-darwin" "aarch64-darwin"];

  in eachSystem supportedSystems (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in
      rec {
        packages.default = packages.ztags;
        packages.ztags = pkgs.stdenvNoCC.mkDerivation {
          name = "ztags";
          src = self;
          nativeBuildInputs = with pkgs; [ zig ];
          buildPhase = ''
            export ZIG_GLOBAL_CACHE_DIR=$TMPDIR/zig
            zig build --release --verbose -p $out
          '';
          dontInstall = true;
        };
      }
    );
}
